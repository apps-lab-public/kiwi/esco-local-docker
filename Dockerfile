FROM openjdk:8-jre

RUN apt-get install -y unzip
RUN useradd -m esco

WORKDIR /home/esco
USER esco
#ENV FUSEKI_HOME=Local_API_v0_19_4/tomcat-esp-api-v03_94/

COPY ["de_ESCO Local API v1.1.0.zip", "./esco.zip"]
RUN unzip esco.zip
RUN rm -rf esco.zip


WORKDIR Local_API_v0_19_4/tomcat-esp-api-v03_94/bin

ENTRYPOINT ["bash","catalina.sh","run"]
EXPOSE 8080
