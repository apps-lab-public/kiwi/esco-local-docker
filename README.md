# Esco Docker Image

This is the repository for the esco-docker image you can find more information about esco here:

https://ec.europa.eu/esco/portal/api

# Build and run locally:

1. Install Docker
2. docker build -t esco:Tagname
3. docker run -p 8080:8080 esco:Tagname

# Access Documentation

http://127.0.0.1:8080/doc/esco_api_doc.html

# Deploy to openshift/okd 

Please see the deployment section to do a non local deployment.